package com.liuevansneale.writeaway;

/*
 * This is the service that interacts with the main activity. When the away message is activated,
 * this service will be started. It is the main service for returning text messages, as well as
 * other services such as Twitter, Foursquare, Facebook.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.liuevansneale.writeaway.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class AwayService extends Service implements LocationListener {
	
	/*
	 * Define all state-dependent variables in this class, not WriteAway so that when the activity restarts
	 * due to orientation changes, these variables will retain their values
	 */
	public static final String TAG = "AwayService";
	//Integers representing what page you're on
	public static final int MAINPAGE = 0;
	public static final int SETTINGSPAGE = 1;
	public static final int ACCOUNTSPAGE = 2;
	public static final int ABOUTPAGE = 3;
	public static final int WEBPAGE = 4;
	
	//Notification Manager for service, shows the icon at the top of screen
	private NotificationManager mNM;
	//Unique ID for notification
	private int NOTIFICATION = R.string.app_name;
	//Map with key = phone#, value = date sent, since don't want texts sent within minInterval
	private ArrayList<TextMessage> sentMessages;
	//Queue of texts waiting to be sent on new location update
	private Queue<String> pendingTexts;
	//gps objects
	protected static LocationManager locationManager;
	private Geocoder geocoder;
	//minInterval(milliseconds) between texts sent to particular person
	protected static long minInterval;
	//locationInterval(milliseconds) between location updates
	protected static long locationInterval;
	public String SENT = "SMS_SENT";
    public String DELIVERED = "SMS_DELIVERED";
    //broadcast receivers to know when texts/calls send/received
	private BroadcastReceiver smsreceiver;
	private PhoneStateListener callreceiver;
	private TelephonyManager telephonymanager;
	private BroadcastReceiver SMSSend;
	private BroadcastReceiver SMSDeliver;
	//if service connected to app, volatile because social media platform classes use it in separate thread
	protected volatile static boolean mIsBound;
	//away message user inputted
	protected static String awayMessage;
	//whether social media applications should update via sms
	protected static boolean useSMS;
	//whether to update social media statuses whenever location received
	protected static boolean updateLocation;
	//whether to add location to awaymessage
	protected static boolean useLocation;
	//provider for location i.e., gps or internet
	protected static String provider;
    //boolean signifying what page app is on
    protected static int currentPage;
    //boolean checking if app was created previously, but restarted due to orientation changed
    protected static boolean appRestarted;
	//stores last location and time gps received it
	protected static LastLocation lastLocation;
	//Handler to run runnablewritetosms class detailed below
	private Handler handler;
	//Social Media Objects
	protected static HashMap<String, SocialMediaInterface> map;
	
	/*
	 * Inner class to store latest GPS location and time it was received
	 */	
	protected class LastLocation {
		private String location;
		private Date date;
		
		public LastLocation(String l, Date d) {
			location = l;
			date = d;
		}
		
		public void setLocation(String l) {
			location = l;
		}
		
		public void setDate(Date d) {
			date = d;
		}
		
		public String getLocation() {
			return location;
		}
		
		public Date getDate() {
			return date;
		}
	}
	
	/*
	 * Innter class to store texts <phoneNumber, time received>
	 */
	private class TextMessage {
		private String phoneNumber;
		private Date date;
		
		public TextMessage(String phoneNumber, Date date) {
			this.phoneNumber = phoneNumber;
			this.date = date;
		}
		
		public String getPhoneNumber() {
			return phoneNumber;
		}
		
		public Date getDate() {
			return date;
		}
	}
	
	/*
	 * Inner class for runnable class to write to inbox (phone has issue of writing to inbox
	 * before received text is written to inbox
	 */
    class RunnableWriteToSMS implements Runnable {
		String phoneNumber;
		String message;
		public RunnableWriteToSMS(String phoneNumber, String message) {
			this.phoneNumber = phoneNumber;
			this.message = message;
		}
		
		public void run() {
	        ContentValues values = new ContentValues();
	        values.put("address", phoneNumber);
	        values.put("body", message);
	        //Put sent sms into your sms history, doesn't work for all phones
	        getContentResolver().insert(Uri.parse("content://sms/sent"), values);
		}
	}
	
	/*
	 * Functions to start the service when app is set to away
	 */
	public class AwayBinder extends Binder {
		AwayService getService() {
			return AwayService.this;
		}
	}
	
	public void onCreate() {
		Log.d(TAG, "locationInterval:" + locationInterval);
		Log.d(TAG, "minInterval:" + minInterval);
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);		
		mIsBound = true;
        geocoder = new Geocoder(this);
    	smsreceiver = new SmsReceiver(this);
    	callreceiver = new CallReceiver(this);
        telephonymanager = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        telephonymanager.listen(callreceiver, PhoneStateListener.LISTEN_CALL_STATE);

    	sentMessages = new ArrayList<TextMessage>();
    	pendingTexts = new LinkedList<String>();
    	handler = new Handler();
    	
        SMSSend = new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                    	handler.post(new Runnable() {
                    		   public void run() {  
                    		      Toast.makeText(getApplicationContext(), "SMS sent", Toast.LENGTH_SHORT).show();  
                    		   }  
                    		});
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    	handler.post(new Runnable() {
                 		   public void run() {  
                 		      Toast.makeText(getApplicationContext(), "Generic Failure", Toast.LENGTH_SHORT).show();  
                 		   }  
                 		});
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                    	handler.post(new Runnable() {
                 		   public void run() {  
                 		      Toast.makeText(getApplicationContext(), "No Service", Toast.LENGTH_SHORT).show();  
                 		   }  
                 		});
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                    	handler.post(new Runnable() {
                 		   public void run() {  
                 		      Toast.makeText(getApplicationContext(), "Null PDU", Toast.LENGTH_SHORT).show();  
                 		   }  
                 		});
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                    	handler.post(new Runnable() {
                 		   public void run() {  
                 		      Toast.makeText(getApplicationContext(), "Radio Off", Toast.LENGTH_SHORT).show();  
                 		   }  
                 		});
                        break;
                }
            }
        };
        
        SMSDeliver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                    	handler.post(new Runnable() {
                 		   public void run() {  
                 		      Toast.makeText(getApplicationContext(), "SMS delivered", Toast.LENGTH_SHORT).show();  
                 		   }  
                 		});
                        break;
                    case Activity.RESULT_CANCELED:
                    	handler.post(new Runnable() {
                 		   public void run() {  
                 		      Toast.makeText(getApplicationContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();  
                 		   }  
                 		});
                        break;                        
                }
            }
        };
        
        registerReceiver(smsreceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    	registerReceiver(SMSSend , new IntentFilter(SENT));
        registerReceiver(SMSDeliver , new IntentFilter(DELIVERED));
		
		//Display notification service is started
		showNotification();
		//retrieve initial location
		if (useLocation) {
			Log.d(TAG, "Location requested");
			for (SocialMediaInterface s : map.values()) {
				s.initialMessage = false;
			}
			locationManager.requestLocationUpdates(provider, locationInterval, 50, this);
		}
		
		for (SocialMediaInterface s : map.values()) {
			if (s.isUsed && !useLocation) {
				s.startService(awayMessage);
				s.initialMessage = true;
			}
		}
	}
	
	public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("AwayService", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }
	
	public void onDestroy() {
        // Cancel the persistent notification.
		Log.d(TAG, "onDestroy appRestarted:" + appRestarted);
        mNM.cancel(NOTIFICATION);
        clear();
        for (SocialMediaInterface s : map.values()) {
        	s.stopService();
        }
        
        // Tell the user we stopped.
        handler.post(new Runnable() {
 		   public void run() {  
 		      Toast.makeText(getApplicationContext(), R.string.awayservice_stopped, Toast.LENGTH_SHORT).show();  
 		   }  
 		});
    }
	
	@Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.
    private final IBinder mBinder = new AwayBinder();
	
	//Show that notification service is started. Requires suppress because of version difference, handled in code
	@SuppressLint("NewApi")
	private void showNotification() {
        
        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, WriteAway.class), 0);
        Notification notification;
            
        if (android.os.Build.VERSION.RELEASE.startsWith("3.")) {
        	notification = new Notification.Builder(this)
            .setContentTitle(getText(R.string.app_name))
            .setContentText(getText(R.string.awayservice_label))
            .setSmallIcon(R.drawable.ic_launcher)
            .setContentIntent(contentIntent)
            .build();
        }
        else {
        	CharSequence text = getText(R.string.app_name);
        	notification = new Notification(R.drawable.ic_launcher, text, System.currentTimeMillis());
        	notification.setLatestEventInfo(this, getText(R.string.awayservice_label),text, contentIntent);
        }
        
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.flags = Notification.FLAG_NO_CLEAR;
        
        // Send the notification.
        mNM.notify(NOTIFICATION, notification);        
    }
	
	/*
	 * Location Functions. TBD what to do in status changed scenarios
	 */

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		if(arg0.equalsIgnoreCase(provider)){
			handler.post(new Runnable() {
		 		   public void run() {  
		 		      Toast.makeText(getApplicationContext(), "GPS is off", Toast.LENGTH_SHORT).show();  
		 		   }  
		 		});
            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
        Log.i(TAG,provider);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
    
	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}
	
    private void clear() {
    	try {
    		unregisterReceiver(smsreceiver);
    		unregisterReceiver(SMSSend);
    		unregisterReceiver(SMSDeliver);
    		telephonymanager.listen(callreceiver, PhoneStateListener.LISTEN_NONE);
    		locationManager.removeUpdates(this);
    	}
    	catch (IllegalArgumentException e) {
    		Log.v(TAG,"Caught IllegalArgumentException in clear");
    	}
    }
    
    /*
     * Texting Functions, called when text is received
     */
    public void sendAway(String phoneNumber, String incomingMessage, Date date) {
		//At least minInterval seconds since last text to same contact, otherwise could go into texting loop
    	Log.v(TAG, "Activity State: Sending AwayText");
		removeOldTexts(date);		
		synchronized (pendingTexts) {
			if (!containsNumber(phoneNumber)) {
				sentMessages.add(new TextMessage(phoneNumber, date));
				if (!useLocation) {
					sendSMS(phoneNumber, awayMessage);
				}
				//if lastlocation not null
				else if(lastLocation != null) {
					sendSMS(phoneNumber, awayMessage + "\n" + lastLocation.getLocation());
				}
				else {
					//text sent when location received in method onLocationChanged
					pendingTexts.add(phoneNumber);
				}
			}	
			pendingTexts.notify();
		}
    }
    
    //Specific to location texts
	@Override
	public void onLocationChanged(Location loc) {
		// TODO Auto-generated method stub
		synchronized (pendingTexts) {
			Log.v(TAG, "Activity State: Location Changed");
			int size = pendingTexts.size();
			String location = getAddress(loc);
			handler.post(new Runnable() {
		 		   public void run() {  
		 		      Toast.makeText(getApplicationContext(), "Location Received", Toast.LENGTH_SHORT).show();  
		 		   }  
		 		});
			if (size > 0) {
				for (int i = 0 ; i < size ; ++i)
					sendSMS(pendingTexts.remove(), awayMessage + "\n" + location );
			}
			if (lastLocation == null)
				lastLocation = new LastLocation(location, new Date());
			else {
				lastLocation.setLocation(location);
				lastLocation.setDate(new Date());
			}
			pendingTexts.notify();
			
			for (SocialMediaInterface s : map.values()) {
				if (s.isUsed) {
					if (!s.initialMessage) {
						s.startService(awayMessage + "\n" + location);
						s.initialMessage = true;
					}
					else if (updateLocation) {
						s.updateStatus(awayMessage + "\n" + location);
					}
				}
			}
		}
	}
	
	private class smsToast implements Runnable {
		private String phoneNumber;
		
		public smsToast(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Toast.makeText(getApplicationContext(), "Sending message to " + contactExists(phoneNumber), Toast.LENGTH_SHORT).show();
		}
	}
    
	/*
	 * Send sms
	 */
    public void sendSMS(String phoneNumber, String message)
    {
    	handler.post(new smsToast(phoneNumber));
        
    	//hack to make phone number start with +1 for country code, for some reason some phones need this
        if(phoneNumber.length() == 10) {
        	phoneNumber = "+1" + phoneNumber;
	    }
        SmsManager sms = SmsManager.getDefault();
        
        if (message.length() <= 160) {
	        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
	            new Intent(SENT), 0);
	 
	        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
	            new Intent(DELIVERED), 0);
	        
	   	 	sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        }
        else {
        	
        	ArrayList<String> parts = sms.divideMessage(message);
        	int numParts = parts.size();
        	ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
        	ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();
        	Intent sendIntent = new Intent(SENT);
        	Intent deliveryIntent = new Intent(DELIVERED);

        	for (int i = 0; i < numParts; i++) {
        		sentIntents.add(PendingIntent.getBroadcast(this, 0, sendIntent , 0));
        		deliveryIntents.add(PendingIntent.getBroadcast(this, 0, deliveryIntent, 0));
        	}

        	sms.sendMultipartTextMessage(phoneNumber, null, parts, sentIntents, deliveryIntents);
        }
        
        handler.postDelayed(new RunnableWriteToSMS(phoneNumber, message), 5000);
    }	

    /*
     * Get contact name
     */
    public String contactExists(String number) {
	    /// number is the phone number
	    Uri lookupUri = Uri.withAppendedPath( PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
	    String[] mPhoneNumberProjection = { PhoneLookup._ID, PhoneLookup.NUMBER, PhoneLookup.DISPLAY_NAME };
	    Cursor cur = getContentResolver().query(lookupUri,mPhoneNumberProjection, null, null, null);
	    try {
	       if (cur.moveToFirst())
	          return cur.getString(cur.getColumnIndex(PhoneLookup.DISPLAY_NAME)) + "(" + number + ")";
	    } finally {
	    if (cur != null)
	       cur.close();
	    }
	    return number;
    }
    
    /*
     * Gets address in readable format from location object
     */
    private String getAddress(Location loc) {
    	String address = "";
		try {
			List<Address> addresses;
			addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 2);
		    for (Address add : addresses) {
		    	address += add.getAddressLine(0) + " ";
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return address;
    }
    
    /*
     * Function to remove texts that were sent more than minInterval seconds ago from the sentMessages ArrayList
     */
    private void removeOldTexts(Date receivedTime) {
    	while(!sentMessages.isEmpty() && receivedTime.getTime() - sentMessages.get(0).getDate().getTime() > minInterval) {
    		sentMessages.remove(0);
    	}
    }
    
    /*
     * Function to see if phoneNumber is in sentMessages
     */
    private boolean containsNumber(String phoneNumber) {
    	for (int i = 0 ; i < sentMessages.size(); i++) {
    		if (sentMessages.get(i).getPhoneNumber().equals(phoneNumber))
    			return true;
    	}
    	return false;
    }
}
