package com.liuevansneale.writeaway;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

/*
 * Abstract class for all the social media platforms. Once the classes for each platform is complete,
 * will remove the SocialMediaData inner class in AwayService and replace with the respective classes
 * Instead of having specific SocialMediaData classes in AwayService, have a map of SocialMediaInterfaces
 * so code can be more generic, for example:
 * we want to extract saved usernames and passwords from internal memory. Currently, code is:
 * twitter.username = settings.getString("twitter.username");
 * ...
 * fsquare.username = settings.getString("fsquare.username");
 * fsquare.password = settings.getString("fsquare.password");
 * 
 * after classes implemented and SocialMediaInterfaces map created, 
 * iterate through the keys of SocialMediaInterfaces, i.e.
 * while (iterator.hasNext)
 * 		String key = iterator.next()
 * 		SocialMediaInterface s = map.getValue(key)
 * 		s.username = settings.getString(key + ".username")
 * 		s.password = settings.getString(key + ".username")
 * 
 * Even easier with the toggle button onClick methods:
 * (map.getValue("Twitter")).toggled()
 */

public abstract class SocialMediaInterface {
	//social media toggled
	protected boolean isUsed;
	//social media's been authenticated and ready to use
	protected boolean authenticated;
	//this is the boolean showing that the intial message on away was hit
	protected boolean initialMessage;
	//boolean knowing if one should send intial text ON if useSMS is true
	protected boolean sendOn;
	//phone number for sms for social media
	protected String SMSNumber;
	protected AwayService servicecontext;
	protected WriteAway activitycontext;
	protected Thread replyThread;
	//for any thread related things
	final Object LOCK = new Object();
	
	public SocialMediaInterface (WriteAway activitycontext, String number) {
		this.activitycontext = activitycontext;
		SMSNumber = number;
	}
	
	/*
	 * Function for toggle buttons onClick Methods
	 */
	public void toggled() {
		isUsed = !isUsed;
	}
	
	/*
	 * Boolean that returns if away message can be started
	 */
	public boolean awayStartAllowed (SharedPreferences pref) {
		return AwayService.useSMS || authenticated || loginAuthorizedUser(pref);
	}
	
	/*
	 * Function to login when accesstoken already stored to internal memory
	 * Returns true if successful found accesstoken and logged in
	 */
	public abstract boolean loginAuthorizedUser(SharedPreferences pref);
	
	/*
	 * ANY CONTENT VIEW CHANGES IN THE FUNCTIONS BELOW TO EITHER MAIN OR SETTINGS PAGE
	 * REQUIRE CALLS TO AWAYSERVICE'S UPDATESETTINGSTOGGLES OR UPDATEMAINTOGGLES FUNCTIONS
	 */
	
	/*
	 * Function to go to URL for authentication
	 * Return true if successfully returned
	 * DONT FORGET TO SET currentPage VARIABLE IN AWAYSERVICE
	 */
	public abstract boolean callAuthenticationURL();
	
	/*
	 * Function to go to URL for SMS setup
	 * DONT FORGET TO SET currentPage VARIABLE IN AWAYSERVICE
	 */
	public abstract void callSMSURL();
	
	/*
	 * AwayService calls this function to start everything: status update, checking direct messages	
	 */
	public void startService(String awayMessage) {
		if (AwayService.useSMS && sendOnText()) {
			servicecontext.sendSMS(SMSNumber, "ON");
		}
		updateStatus(awayMessage);
		checkDirectMessages(awayMessage);
	}
	
	/*
	 * AwayService calls this function to stop everything
	 */
	public void stopService() {
		if (AwayService.useSMS && sendOn) {
			servicecontext.sendSMS(SMSNumber, "OFF");
		}
		synchronized (LOCK) {
			if (replyThread != null)
				replyThread.interrupt();
			LOCK.notify();
		}
	}
	
	/*
	 * Functions for activity within social media platform. awayMessage parameter should be awayMessage
	 * in AwayService concatenated with latest location if useLocation true. If isUsed is false then
	 * nothing should happen in every function below. 
	 * 
	 * When user hits away, updateStatus and checkDirectMessages immediately called.
	 * parseIncoming should be called in AwayService.sendAway for each socialmediainterface, if false for every
	 * then we know it's a text from a person not social media platform, and respond.
	 * 
	*/
	
	/*
	 * Function to update status for specific social media platform via data or sms depending on useSMS
	 */
	public abstract boolean updateStatus(String awayMessage);
	
	/*
	 * Function to parse incoming text. If phoneNumber corresponds to number from a social media
	 * platform, parse message and respond appropriately (will need further details from Malcolm)
	 * For example on twitter, if someone pms you and you receive a text, pm back with message ONLY 
	 * IF useSMS ACTIVATED
	 * Same with if someone on facebook chats you
	 * 
	 * Need to decide what to do for texts regarding groups/tags/etc on facebook, for now just ignore
	 * 
	 * Returns true if incoming text is related to this social media platform
	 */
	public abstract boolean parseIncoming(String phoneNumber, String incomingMessage, String awayMessage);
	
	
	/*
	 * Function to handle direct messages received through data only (parseIncoming handles through SMS)
	 * Run on separate thread , thread will be in a while(AwayService.mIsBound)
	 *  loop (which is why mIsBound is volatile and static variable in AwayService) and thread sleeps for 
	 *  AwayService.minInterval seconds before checking for direct messages in Twitter or chat messages in Facebook
	 *  
	 *  Function should do nothing if useSMS true
	 */
	public abstract void checkDirectMessages(String awayMessage);
	
	/*
	 * Username that is logged in, otherwise empty string
	 */
	public abstract String getUsername();
	
	/*
	 * Function to know if ON text should be sent
	 * Scrapes sent texts
	 */
	public boolean sendOnText() {
		sendOn = true;
		try {
			Uri mSmsinboxQueryUri = Uri.parse("content://sms/sent");
			Cursor cursor = activitycontext.getContentResolver().query(mSmsinboxQueryUri,new String[] {"body, address"},"(UPPER(body)=UPPER('on') or UPPER(body)=UPPER('off'))", null, "date DESC");
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				String address = cursor.getString(cursor.getColumnIndex("address")).replace("-", "").toUpperCase();
				while (!cursor.isLast() && address.compareTo(SMSNumber.toUpperCase())!=0) {
					cursor.moveToNext();
					address = cursor.getString(cursor.getColumnIndex("address")).replace("-", "").toUpperCase();
				}
				if (address.compareTo(SMSNumber.toUpperCase())==0 && cursor.getString(cursor.getColumnIndex("body")).toUpperCase().compareTo("ON")==0)
					sendOn = false;
			}
			if (cursor != null) {
				cursor.close();
			}
		}
		catch (Exception e) {
		}
		return sendOn;
	}
	
}
