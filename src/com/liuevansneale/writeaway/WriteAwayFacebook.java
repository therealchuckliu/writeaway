package com.liuevansneale.writeaway;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.facebook.android.Facebook;
import com.liuevansneale.writeaway.util.Constants;


public class WriteAwayFacebook extends SocialMediaInterface{

	private Facebook mFacebook = new Facebook(Constants.APP_ID);
	
	public WriteAwayFacebook(WriteAway activitycontext){
		super(activitycontext, Constants.FACEBOOK_NUMBER);
	}
	
	public WriteAwayFacebook(WriteAway activitycontext, Facebook mFacebook) {
		super(activitycontext, Constants.FACEBOOK_NUMBER);
		this.mFacebook = mFacebook;
	}

	public void setFacebookObject(Facebook mFacebook){
		this.mFacebook = mFacebook;
	}
	
	@Override
	public boolean loginAuthorizedUser(SharedPreferences pref) {
		if(pref.contains(Constants.FACEBOOK_ACCESS_TOKEN)){
			if(pref.contains(Constants.FACEBOOK_ACCESS_EXPIRES)){
				mFacebook.setAccessExpires(pref.getLong(Constants.FACEBOOK_ACCESS_EXPIRES, 0));
				mFacebook.setAccessToken(pref.getString(Constants.FACEBOOK_ACCESS_TOKEN, null));
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean callAuthenticationURL() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void callSMSURL() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean updateStatus(String awayMessage) {
		String sAccessToken = mFacebook.getAccessToken();
		Bundle params = new Bundle();
		params.putString("access_token", sAccessToken);
		params.putString("message", awayMessage);
		if(AwayService.useSMS){
			servicecontext.sendSMS(SMSNumber, awayMessage);
			return true;
		}else{
			try {
				mFacebook.request("me/feed", params, "POST");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return true;
		}
	}

	@Override
	public boolean parseIncoming(String phoneNumber, String incomingMessage,
			String awayMessage) {
		return false;
	}

	@Override
	public void checkDirectMessages(String awayMessage) {
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
