package com.liuevansneale.writeaway;

import java.util.Date;
import java.util.ArrayList;

import android.database.Cursor;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class CallReceiver extends PhoneStateListener{

	AwayService contextActivity;
	static final Object LOCK = new Object();
	ArrayList<IncomingCall> incomingCalls;
	static volatile boolean idleState;
	Thread t;
	
	private class IncomingCall {
		String number;
		long time;
		
		public IncomingCall(String number, long time) {
			this.number = number;
			this.time = time;
		}
	}
	
	public CallReceiver(AwayService activity) {
        this.contextActivity = activity;
        incomingCalls = new ArrayList<IncomingCall>();
    	idleState = true;
    }
	
	public void onCallStateChanged(int state, String incomingNumber) {
		try {
	        switch(state){
	            case TelephonyManager.CALL_STATE_RINGING:
	            	synchronized (LOCK) {
	            		idleState = false;
	            		incomingCalls.add(0,new IncomingCall(incomingNumber.replace("-", "").replace("+", ""), (new Date()).getTime()));
	            		if (t == null || !t.isAlive()) {
	            			t = new WaitThread();
	    	            	t.start();
	            		}
	            		LOCK.notify();
	            	}
	                break;
	            case TelephonyManager.CALL_STATE_OFFHOOK:
	            	break;
	            case TelephonyManager.CALL_STATE_IDLE:
	            	synchronized(LOCK) {
	            		idleState = true;
	            		t.interrupt();
	            		LOCK.notify();
	            	}
	                break;
	        }
		}
		catch (Exception e) {
			
		}
		finally {
			super.onCallStateChanged(state, incomingNumber);
		}
    }
	
	class WaitThread extends Thread {
		public void run() {
			try {
				Thread.sleep(Long.MAX_VALUE);
			} catch (Exception e) {
				//check if missed call is in call log
				synchronized (LOCK) {
				    Cursor callCursor = contextActivity.getContentResolver().query(android.provider.CallLog.Calls.CONTENT_URI, new String[] {android.provider.CallLog.Calls.NUMBER, android.provider.CallLog.Calls.DATE}, android.provider.CallLog.Calls.TYPE + "=" + android.provider.CallLog.Calls.MISSED_TYPE + " AND " + android.provider.CallLog.Calls.DATE + ">=" + ((new Date()).getTime()-60000), null, android.provider.CallLog.Calls.DATE + " DESC");
					int i = -1;
				    if (callCursor.moveToFirst()) {
						do {
							//ith most recent missed call in history. must be at least ith index in incomingCalls arraylist
							i++;
							String number = callCursor.getString(callCursor.getColumnIndex(android.provider.CallLog.Calls.NUMBER));
							long time = callCursor.getLong(callCursor.getColumnIndex(android.provider.CallLog.Calls.DATE));
							long currTime = (new Date()).getTime()-30000;
							int j = i;
							while (j < incomingCalls.size() && incomingCalls.get(j).time <= time && !incomingCalls.get(j).number.equalsIgnoreCase(number)) {
								j++;
							}
							if (j < incomingCalls.size() && incomingCalls.get(j).number.equalsIgnoreCase(number)) {
								contextActivity.sendAway(incomingCalls.get(i).number, "Phone call", new Date());
							}
						} while (callCursor.moveToNext());
	
					}
				    if (callCursor != null) {
				    	callCursor.close();
				    }
				    incomingCalls.clear();
				    LOCK.notify();
				}
			}
		}
	}

	
}
