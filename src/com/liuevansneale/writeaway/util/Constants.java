package com.liuevansneale.writeaway.util;

public class Constants {
	public static final String APP_ID = "461107073910434";
	public static final String[] FACEBOOK_PERMISSIONS = {"publish_stream", "read_mailbox"};
    public static final int FACEBOOK_CODE = 1010;
    public static final String FACEBOOK_ACCESS_EXPIRES = "access_expires";
    public static final String FACEBOOK_NUMBER = "32665";
    public static final String FACEBOOK_ACCESS_TOKEN = "access_token";
    public static final String twitter = "twitter";
    public static final String facebook = "facebook";
	//internal memory storage keys
	public static final String PREF_ACCESS_TOKEN = "accessToken";
	public static final String PREF_ACCESS_TOKEN_SECRET = "accessTokenSecret";
	//the string in this URL should be consistent with the scheme value in the manifest
	public static final String CALLBACK_URL = "twitterauthentication:///";
    //Consumer key received from registering app on Twitter
	public static final String CONSUMER_KEY = "UXlIDovNq0eVPDonv83Ew";
    //Consumer secret received from reigstering app on Twitter
	public static final String CONSUMER_SECRET = "VvGoPmJmm2S9gYyKakkR098PSF00TTOr0wG7zaHMcnQ";
	public static final String TWITTER_NUMBER = "40404";
}
