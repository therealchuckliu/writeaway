package com.liuevansneale.writeaway;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.Facebook.ServiceListener;
import com.facebook.android.FacebookError;
import com.liuevansneale.writeaway.util.Constants;

public class WriteAway extends Activity {

	public static final String TAG = "WriteAway";
	private Criteria c;
	private Intent serviceIntent;
	protected SharedPreferences settings;
	protected SharedPreferences.Editor editor;	
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private Resources res;
    private Facebook mFacebook = new Facebook(Constants.APP_ID);
    
    /* 
     * Methods for interaction between service and activity
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
    		//make sure the local service variable is set in twitter once service connected
        	for (SocialMediaInterface s : AwayService.map.values()) {
        		s.servicecontext = ((AwayService.AwayBinder)service).getService();
        	}
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
        	dialog.dismiss();
    	    stopService(serviceIntent);
            Toast.makeText(WriteAway.this, R.string.awayservice_disconnected,
                    Toast.LENGTH_SHORT).show();
        }
    };

    void doBindService() {
        // Establish a connection with the service.  
    	// Must set usernames and passwords to what is saved on phone
    	getSavedData();
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    void doUnbindService() {
        if (AwayService.mIsBound) {
            // Detach our existing connection.
            unbindService(mConnection);
        }
    }

    @Override
    protected void onDestroy() {
    	Log.d(TAG, "onDestroy");
        super.onDestroy();
        doUnbindService();
    }
    
    /*
     * Methods for WriteAway Application
     */
	
    /** Called when the activity is first created. */
    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "Activity State: onCreate()");
        super.onCreate(savedInstanceState);
        //new functionality in android that restricts certain calls that twitter4j uses
        if (android.os.Build.VERSION.SDK_INT > 9) {
        	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build(); StrictMode.setThreadPolicy(policy);
        }
        settings = getSharedPreferences(TAG, 0);
        editor = settings.edit();
    	res = getResources();
    	
    	String access_token = settings.getString(Constants.FACEBOOK_ACCESS_TOKEN, null);
        long expires = settings.getLong(Constants.FACEBOOK_ACCESS_EXPIRES, 0);
        if(access_token != null) {
            mFacebook.setAccessToken(access_token);
        }
        if(expires != 0) {
            mFacebook.setAccessExpires(expires);
        }
    	
        //it's possible this activity was destroyed by the OS, but the service is still running
    	//update: configuration changes don't restart the app anymore, but os can still kill the process
        if (!AwayService.appRestarted) {
        	AwayService.map = new HashMap<String, SocialMediaInterface>();
        	AwayService.map.put(Constants.twitter, new Twitter(this));
        	AwayService.map.put(Constants.facebook, new WriteAwayFacebook(this, mFacebook));
        	AwayService.minInterval = Long.parseLong(res.getString(R.string.defaultInterval)) * 60000;
        	AwayService.locationInterval = Long.parseLong(res.getString(R.string.defaultInterval)) * 60000;
        	getSavedData();
        }
        if (AwayService.currentPage == AwayService.SETTINGSPAGE) {
        	setContentView(R.layout.settings);
        	updateSettingsToggles(); 
        }
        else {       
        	setContentView(R.layout.main);
        	updateMainToggles();	
        }
        serviceIntent = new Intent(WriteAway.this, AwayService.class);
        
	    if (builder == null) {
	    	builder = new AlertDialog.Builder(this);
	    	builder.setMessage("Return from away?")
	       .setCancelable(false)
	       .setNeutralButton("Back", new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        	    Log.d(TAG, "onClick");
					doUnbindService();
					AwayService.mIsBound = false;
					stopService(serviceIntent);
					dialog.dismiss();
	           }
	        })
	        .setOnCancelListener(new OnCancelListener() {
	    	   	public void onCancel(DialogInterface dialog) {
	    	   		Log.d(TAG, "onCancel");
	        	    doUnbindService();
					AwayService.mIsBound = false;
	        	    stopService(serviceIntent);
	    	   	}
	        });
	    }
	    if (!AwayService.appRestarted)
	    	AwayService.locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        //Criteria for location
        c = new Criteria();
        c.setAltitudeRequired(false);
        c.setBearingRequired(false);
        c.setSpeedRequired(false);
        c.setCostAllowed(false);
        c.setPowerRequirement(Criteria.POWER_LOW);
        c.setAccuracy(Criteria.ACCURACY_COARSE);
    	AwayService.provider = AwayService.locationManager.getBestProvider(c, false);
	    if (AwayService.mIsBound) {
	    	//just show the dialog and bind the activity to the service
	    	doBindService();
	    }
	    AwayService.appRestarted = true;
    } 
    
    /*
     * When away is clicked, set up the service
     */
    public void isAway(View v) {
    	AwayService.awayMessage = ((EditText) findViewById(R.id.editAway)).getText().toString();
    	if (AwayService.awayMessage.length() < 140 && AwayService.awayMessage.length() > 0) {
    		AwayService.provider = AwayService.locationManager.getBestProvider(c, true);
    		dialog = builder.show();
    		startService(serviceIntent);
    		doBindService();
    	}
    	else {
    		Toast.makeText(this, 
                    "Please enter a message less than 140 characters", 
                    Toast.LENGTH_SHORT).show();
    	}
    }
    
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == Constants.FACEBOOK_CODE){
        	mFacebook.authorizeCallback(requestCode, resultCode, data);
        	((WriteAwayFacebook)AwayService.map.get(Constants.facebook)).setFacebookObject(mFacebook);
        	editor.putString(Constants.FACEBOOK_ACCESS_TOKEN, mFacebook.getAccessToken());
        	editor.putLong(Constants.FACEBOOK_ACCESS_EXPIRES, mFacebook.getAccessExpires());
        }
    }
    
    
    public void onResume() {    
        super.onResume();
        mFacebook.extendAccessTokenIfNeeded(this, new ServiceListener() {
			
			@Override
			public void onFacebookError(FacebookError e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onError(Error e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onComplete(Bundle values) {
				editor.putString(Constants.FACEBOOK_ACCESS_TOKEN, mFacebook.getAccessToken());
				editor.putLong(Constants.FACEBOOK_ACCESS_EXPIRES, mFacebook.getAccessExpires());
				editor.commit();
			}
		});
    }
    
    /*
     * Go to the settings page
     */
    public void settingsPage(View v) {
    	settingsPageHelper();
    }
    
    public void settingsPageHelper() {
    	if (AwayService.currentPage == AwayService.MAINPAGE)
    		AwayService.awayMessage = ((EditText) findViewById(R.id.editAway)).getText().toString();
    	setContentView(R.layout.settings);
    	AwayService.currentPage = AwayService.SETTINGSPAGE;
    	updateSettingsToggles();
    }
    
    /*
     * Go to the main page
     */
    public void mainPage(View v) {
    	getEditData();
    	setContentView(R.layout.main);
    	AwayService.currentPage = AwayService.MAINPAGE;
    	updateMainToggles();
    }
    
    /*
     * Go to the accounts page
     */
    public void accountsPage(View v) {
    	getEditData();
    	setContentView(R.layout.accounts);
    	AwayService.currentPage = AwayService.ACCOUNTSPAGE;
    	updateAccountToggles();
    }
    
    /*
     * Go to the about page
     */
    public void aboutPage(View v) {
    	getEditData();
    	setContentView(R.layout.about);
    	AwayService.currentPage = AwayService.ABOUTPAGE;
    }
    
    /*
     * When location button is clicked, make sure provider enabled and update picture
     */
    public void addLocation(View v) {
    	AwayService.useLocation = !AwayService.useLocation;
    	if (AwayService.useLocation) {
    		if (!AwayService.locationManager.isProviderEnabled( AwayService.provider )) {
        		showProviderDialog();
    		}
    	}
    }
    
    /*
     * Update twitter status when away
     */
    public void addTwitter(View v) {
    	AwayService.map.get(Constants.twitter).toggled();
    	if (!AwayService.map.get(Constants.twitter).awayStartAllowed(settings)) {
    		AwayService.map.get(Constants.twitter).toggled();
    		((ToggleButton)findViewById(R.id.buttonTwitter)).setChecked(false);
    		settingsPageHelper();
        	Toast.makeText(this, "Make sure Twitter is authenticated or linked via SMS",
                    Toast.LENGTH_SHORT).show();
		}
    }
    
    /*
     * Update facebook status when away
     */
    public void addFacebook(View v) {
    	AwayService.map.get(Constants.facebook).toggled();
    }
    
    /*
     * Update social media through SMS
     */
    public void addSMS(View v) {
    	AwayService.useSMS = !AwayService.useSMS;
    }
    
    /*
     * Update social media platforms on new locations
     */
    public void updateLocation(View v) {
    	AwayService.updateLocation = !AwayService.updateLocation;
    }
    
    /*
     * Update Facebook login information
     */
    public void updateFacebookInfo(View v) {
    	Toast.makeText(this, 
                "Updated Facebook user data", 
                Toast.LENGTH_SHORT).show();
//    	AwayService.map.get(Constants.facebook).username = ((EditText)((LinearLayout) findViewById(R.id.facebookdata)).findViewById(R.id.username)).getText().toString();
//    	AwayService.map.get(Constants.facebook).password = ((EditText)((LinearLayout) findViewById(R.id.facebookdata)).findViewById(R.id.password)).getText().toString();
//    	writeUserData("facebook", AwayService.map.get(Constants.facebook).username, AwayService.map.get(Constants.facebook).password);
    }
    
    /*
     * Write username and passwords to internal memory
     */
    private void writeUserData(String socialmedia, String username, String password) {
    	editor.putString(socialmedia + ".username", username);
    	editor.putString(socialmedia + ".password", password);
    	editor.commit();
    }
    
    /*
     * Set social media usernames and passwords to what is saved in internal memory
     */
    public void getSavedData() {
    	for (SocialMediaInterface s : AwayService.map.values()) {
    		s.loginAuthorizedUser(settings);
    	}  
    }
    
    /*
     * Override function for right before the activity is restarted
     */
    protected void onSaveInstanceState (Bundle outState) {
    	if (AwayService.currentPage == AwayService.SETTINGSPAGE)
    		getEditData();
    	else if (AwayService.currentPage == AwayService.MAINPAGE)
    		AwayService.awayMessage = ((EditText)findViewById(R.id.editAway)).getText().toString();
    }
    
    /*
     * Function for back button
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the BACK key and not on main page
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	if (AwayService.currentPage == AwayService.ABOUTPAGE || AwayService.currentPage == AwayService.ACCOUNTSPAGE) {
        		AwayService.currentPage = AwayService.SETTINGSPAGE;
        		setContentView(R.layout.settings);
        		updateSettingsToggles();
        	}
        	else if (AwayService.currentPage == AwayService.SETTINGSPAGE) {
        		AwayService.currentPage = AwayService.MAINPAGE;
        		getEditData();
        		setContentView(R.layout.main);
        		updateMainToggles();
        	}
        	else if (AwayService.currentPage == AwayService.WEBPAGE) {
        		AwayService.currentPage = AwayService.ACCOUNTSPAGE;
        		setContentView(R.layout.accounts);
        		updateAccountToggles();
        	}
            return true;
        }
        // If it wasn't the BACK key or on the main page do default
        return super.onKeyDown(keyCode, event);
    }   
    
    /*
     * Save what is written on settings page for when onsaveinstancestate is called
     */
    private void getEditData() {
    	try {
    		AwayService.minInterval = Long.parseLong(((EditText)findViewById(R.id.minInterval)).getText().toString()) * 60000;
    		AwayService.locationInterval = Long.parseLong(((EditText)findViewById(R.id.locationInterval)).getText().toString()) * 60000;
    	}
    	catch (NumberFormatException e) {
    		AwayService.minInterval = Long.parseLong(res.getString(R.string.defaultInterval)) * 60000;
    		AwayService.locationInterval = Long.parseLong(res.getString(R.string.defaultInterval)) * 60000;
    	}
    	if (AwayService.minInterval <= 0 || AwayService.locationInterval <= 0) {
    		Toast.makeText(this, 
                    "Interval inputs must be integers greater than 0", 
                    Toast.LENGTH_SHORT).show();  
    		AwayService.minInterval = Long.parseLong(res.getString(R.string.defaultInterval)) * 60000;
    		AwayService.locationInterval = Long.parseLong(res.getString(R.string.defaultInterval)) * 60000;
    	}
    }
    
    /*
     * Update toggles for main page options
     */
    public void updateMainToggles() {
		if (AwayService.awayMessage != null)
			((EditText)findViewById(R.id.editAway)).setText(AwayService.awayMessage);
    	((ToggleButton)findViewById(R.id.buttonLocation)).setChecked(AwayService.useLocation);
    	((ToggleButton)findViewById(R.id.buttonTwitter)).setChecked(AwayService.map.get(Constants.twitter).isUsed);
    	((ToggleButton)findViewById(R.id.buttonFacebook)).setChecked(AwayService.map.get(Constants.facebook).isUsed);
    }
    
    /*
     * Update checkboxes for settings page
     */
    public void updateSettingsToggles() {
    	((EditText)findViewById(R.id.minInterval)).setText(Long.toString(AwayService.minInterval/60000));
    	((EditText)findViewById(R.id.locationInterval)).setText(Long.toString(AwayService.locationInterval/60000));
    	((CheckBox)findViewById(R.id.usesms)).setChecked(AwayService.useSMS);
    	((CheckBox)findViewById(R.id.updateLocation)).setChecked(AwayService.updateLocation);
    }
    
    /*
     * Update accounts information on accounts page
     */
    public void updateAccountToggles() {
    	Button mFacebookButton = ((Button)(findViewById(R.id.facebookdata).findViewById(R.id.facebook_authenticate)));
    	((Button)((LinearLayout)findViewById(R.id.twittersetup)).findViewById(R.id.smssetup)).setOnClickListener(twittersms);
    	((Button)((LinearLayout)findViewById(R.id.twittersetup)).findViewById(R.id.datasetup)).setOnClickListener(twitterdata);
    	if(mFacebook.isSessionValid()){	
    		mFacebookButton.setText(getResources().getString(R.string.fbook_logout));
    		mFacebookButton.setOnClickListener(new FacebookLogoutListener(this, mFacebookButton));
    	}else{
    		mFacebookButton.setText(getResources().getString(R.string.fbook_login));
    		mFacebookButton.setOnClickListener(new FacebookLoginListener(this, mFacebookButton));
    	}
    }
    
    View.OnClickListener twittersms = new View.OnClickListener() {
        public void onClick(View v) {
        	AwayService.map.get(Constants.twitter).callSMSURL();
        }
     };
     View.OnClickListener twitterdata = new View.OnClickListener() {
         public void onClick(View v) {
        	if (AwayService.map.get(Constants.twitter).authenticated) {
        		AlertDialog.Builder twitteraccount = new AlertDialog.Builder(WriteAway.this);
    	    	twitteraccount.setMessage("You are already logged into username:" + 
        		AwayService.map.get(Constants.twitter).getUsername() + ". Pressing yes will log you out. Continue?")
    	       .setCancelable(false)
    	       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	    AwayService.map.get(Constants.twitter).callAuthenticationURL();
    	           }
    	        })
    	       .setNegativeButton("No", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	    dialog.dismiss();
    	           }
    	        });
    	    	twitteraccount.show();
        	}
        	else
        		AwayService.map.get(Constants.twitter).callAuthenticationURL();
         }
       };
       
    protected void onNewIntent(Intent intent) {
    	super.onNewIntent(intent);
    	if (AwayService.map.get(Constants.twitter) != null) {
    		((Twitter)AwayService.map.get(Constants.twitter)).onNewIntent(intent);
    	}
    }

    
    public void showProviderDialog() {
		Message msg = handler.obtainMessage();
		msg.arg1 = 1;
		handler.sendMessage(msg);
	}

	//When GPS is turned off alert dialog comes up asking if you want to enable it
	private final Handler handler = new Handler() {
		public void handleMessage(Message msg){  
			if (msg.arg1 == 1 && !isFinishing()) {
				AlertDialog.Builder builder = new AlertDialog.Builder(WriteAway.this);  
				builder.setMessage("Your GPS is disabled. Would you like to enable it?")  
				     .setCancelable(false)  
				     .setPositiveButton("Yes",  
				          new DialogInterface.OnClickListener(){  
				          public void onClick(DialogInterface dialog, int id){
				              startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));  
				          }  
				     });  
				     builder.setNegativeButton("No",  
				          new DialogInterface.OnClickListener(){  
				          public void onClick(DialogInterface dialog, int id){  
				               dialog.cancel();  
				          }  
				     });  
				AlertDialog alert = builder.create();  
				alert.show();  
			}
		}
	};
	
	private class FacebookLogoutListener implements OnClickListener{
		private Button mButton;
		private Context mContext;
		
		public FacebookLogoutListener(Context mContext, Button mButton){
			this.mButton = mButton;
			this.mContext = mContext;
		}
		
		@Override
		public void onClick(View v) {
			ProgressDialog mProgDialog = new ProgressDialog(mContext);
			mProgDialog.setTitle("Logging Out...");
			mProgDialog.show();
			editor.remove(Constants.FACEBOOK_ACCESS_TOKEN).remove(Constants.FACEBOOK_ACCESS_EXPIRES);
			if(editor.commit()){
				AwayService.map.get(Constants.facebook).authenticated = false;
				((WriteAwayFacebook)AwayService.map.get(Constants.facebook)).setFacebookObject(mFacebook);
				mButton.setText(res.getString(R.string.fbook_login));
				mFacebook.setAccessExpiresIn("1");
			}else{
				mProgDialog.dismiss();
				Toast.makeText(mContext, "Error logging you out of facebook", Toast.LENGTH_SHORT).show();
				return;
			}
			mProgDialog.dismiss();
			settingsPage(null);
		}
		
	}
	
	private class FacebookLoginListener implements OnClickListener{

		private Activity mActivity;
		private Button mButton;
		
		public FacebookLoginListener(Activity mActivity, Button mButton){
			this.mActivity = mActivity;
			this.mButton = mButton;
		}
		
		@Override
		public void onClick(View v) {
			mFacebook.authorize(mActivity, Constants.FACEBOOK_PERMISSIONS, Constants.FACEBOOK_CODE, new DialogListener() {
				
				@Override
				public void onFacebookError(FacebookError e) {
					Log.e("error authorizing facebook", e.getMessage());
				}
				
				@Override
				public void onError(DialogError e) {
					
				}
				
				@Override
				public void onComplete(Bundle values) {
					AwayService.map.get(Constants.facebook).authenticated = true;
					editor.putString(Constants.FACEBOOK_ACCESS_EXPIRES, mFacebook.getAccessToken());
                    editor.putLong("access_expires", mFacebook.getAccessExpires());
                    editor.commit();
                    mButton.setText(res.getString(R.string.fbook_logout));
				}
				
				@Override
				public void onCancel() {
					
				}
			});
		}
		
	}
}