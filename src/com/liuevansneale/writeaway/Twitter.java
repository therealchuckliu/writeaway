package com.liuevansneale.writeaway;

import java.util.Date;
import com.liuevansneale.writeaway.util.Constants;
import java.util.Iterator;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;
import twitter4j.*;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class Twitter extends SocialMediaInterface {
	//class from twitter4j jar file
	private twitter4j.Twitter twitter;
    private RequestToken reqToken;

	
	public Twitter(WriteAway activitycontext) {
		// TODO Auto-generated constructor stub
		super(activitycontext, Constants.TWITTER_NUMBER);
		twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
	}

	@Override
	public boolean loginAuthorizedUser(SharedPreferences pref) {
		// TODO Auto-generated method stub
		if (pref.contains(Constants.PREF_ACCESS_TOKEN)) {
			String token = pref.getString(Constants.PREF_ACCESS_TOKEN, null);
	        String secret = pref.getString(Constants.PREF_ACCESS_TOKEN_SECRET, null);

	        AccessToken at = new AccessToken(token, secret);
	        twitter.setOAuthAccessToken(at);
	        authenticated = true;
	        return true;
		}
		return false;		
	}

	@Override
	public boolean updateStatus(String awayMessage) {
		// TODO Auto-generated method stub
		if (AwayService.useSMS) {
			servicecontext.sendSMS(SMSNumber, awayMessage);
			return true;
		}
		else if (authenticated) {
			try {
				twitter.updateStatus(awayMessage);
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean parseIncoming(String phoneNumber, String incomingMessage,
			String awayMessage) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void checkDirectMessages(String awayMessage) {
		// TODO Auto-generated method stub
		if (!AwayService.useSMS) {
			replyThread = new CheckMentions(awayMessage);
			replyThread.start();
		}
	}
	
	/*
	 * Runnable class that checkDirectMessages will run on a separate thread
	 */
    class CheckMentions extends Thread {
		String awayMessage;
		public CheckMentions(String awayMessage) {
			this.awayMessage = awayMessage;
		}
		
		public void run() {
	        while (AwayService.mIsBound) {
	        	try {
					Thread.sleep(AwayService.minInterval);
	        		Iterator<Status> it = twitter.getMentions().iterator();
	        		long currTime = new Date().getTime();
	        		Status status;
	        		while(it.hasNext()) {
	        			status = it.next();
	        			if (currTime - status.getCreatedAt().getTime() < AwayService.minInterval) {
	        				synchronized (LOCK) {
	        					if (Thread.interrupted()) {
	        						throw new InterruptedException();
	        					}
	        					twitter.updateStatus(new StatusUpdate("@" + status.getUser().getScreenName() + " " + awayMessage).inReplyToStatusId(status.getId()));
	        					LOCK.notify();
	        				}
	        			}
	        		}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					LOCK.notify();
					return;
				}
	        }
		}
	}
	
	public void callSMSURL() {
		WebView SMSSite = new WebView(activitycontext);
		SMSSite.loadUrl("https://support.twitter.com/articles/14589-how-to-add-your-phone-via-sms#add-phone-sms");
		SMSSite.getSettings().setSupportZoom(true);
		AwayService.currentPage = AwayService.WEBPAGE;
		activitycontext.setContentView(SMSSite);
	}

	@Override
	public boolean callAuthenticationURL() {
		// TODO Auto-generated method stub
		try {
			//need to destroy all information of authentication
			if (reqToken != null) {
				activitycontext.editor.remove(Constants.PREF_ACCESS_TOKEN);
				activitycontext.editor.remove(Constants.PREF_ACCESS_TOKEN_SECRET);
				activitycontext.editor.commit();
				twitter = new TwitterFactory().getInstance();
				twitter.setOAuthConsumer(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
			}
			authenticated = false;
			reqToken = twitter.getOAuthRequestToken(Constants.CALLBACK_URL);
			WebView twitterSite = new WebView(activitycontext);
			twitterSite.getSettings().setSupportZoom(true);
			twitterSite.requestFocus(View.FOCUS_DOWN);
			twitterSite.setOnTouchListener(new View.OnTouchListener() {
				
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_UP:
					if (!v.hasFocus()) {
						v.requestFocus();
					}
					break;
					}
						return false;
					}
			});
			twitterSite.loadUrl(reqToken.getAuthenticationURL());
			AwayService.currentPage = AwayService.WEBPAGE;
			activitycontext.setContentView(twitterSite);
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			return false;
		}
		return true;
	}
	
	public boolean onNewIntent(Intent intent) {
		Uri uri = intent.getData();
		boolean success = true;
        if (uri != null && uri.toString().startsWith(Constants.CALLBACK_URL)) { // If the user has just logged in
                try {
                    String oauthVerifier = uri.getQueryParameter("oauth_verifier");
                    AccessToken at = twitter.getOAuthAccessToken(reqToken, oauthVerifier);
                    twitter.setOAuthAccessToken(at);
                    String token = at.getToken();
                    String secret = at.getTokenSecret();
                    activitycontext.editor.putString(Constants.PREF_ACCESS_TOKEN, token);
                    activitycontext.editor.putString(Constants.PREF_ACCESS_TOKEN_SECRET, secret);
                    activitycontext.editor.commit();
                    authenticated = true;
		        } 
                catch (Exception e) {
		                success = false;
		        }
                finally {
        			AwayService.currentPage = AwayService.ACCOUNTSPAGE;
                    activitycontext.setContentView(R.layout.accounts);
                    activitycontext.updateAccountToggles();
                    if (success)
                    	Toast.makeText(activitycontext, "Successfully authenticated",
                            Toast.LENGTH_SHORT).show();
                    else
                    	Toast.makeText(activitycontext, "Unsuccessfully authenticated",
                                Toast.LENGTH_SHORT).show();
                }
        }
        return success;
	}
	
	public String getUsername() {
		String username = "";
		if (authenticated) {
			try {
				username = twitter.getScreenName();
			} catch (Exception e) {
				username = "";
			}
		}
		return username;
	}

}
