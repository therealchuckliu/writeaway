package com.liuevansneale.writeaway;

import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
 
public class SmsReceiver extends BroadcastReceiver
{
	AwayService contextActivity;
	
	public SmsReceiver(AwayService activity) {
        this.contextActivity = activity;
    }

	/*
	 * Extract phone number from received text
	 */
	@Override
	public void onReceive(Context context, Intent intent)  {	
		Bundle bundle = intent.getExtras();        
        SmsMessage[] msgs = null;         
        if (bundle != null && contextActivity != null)
        {
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];            
            for (int i=0; i<msgs.length; i++){
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                while (!CallReceiver.idleState) {
                	try {
						Thread.sleep(5000);
					} catch (Exception e) {
					}
                }
                contextActivity.sendAway(msgs[i].getOriginatingAddress(), msgs[i].getMessageBody(), new Date());
            }
        }                         
    }
}
